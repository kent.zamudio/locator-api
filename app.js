const express = require('express');
const apiKey = require('./res/auth.js');
const axios = require('axios');

const app = express();

/**
 * Gets the barangay coordinate through Google Place Search
 * @param {str} brgy - barangay from req.query
 * @param {str} municipality - municipality from req.query
 * @param {str} province - province from req.query
 * @return {Object} coordinate object containing latitude and longitude
 */
const placeCoordinates = async (brgy, municipality, province) => {
	const url = `https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=${brgy} ${municipality} ${province}&inputtype=textquery&fields=geometry&key=${apiKey}`;
	return await axios
		.get(url)
		.then(response => {
			return (
				response.data?.candidates?.[0]?.geometry?.location || 'place not found'
			);
		})
		.catch(error => {
			return 'Something went wrong while getting barangay coordinates';
		});
};

/**
 * Gets suggestions based on coordinates and `searchText`, uses Google Place Autocomplete
 * @param {Object} coordinates - coordinates of the search area
 * @param {str} searchText - place to be searched from req.query
 * @return {Object} array containing possible match results
 */
const placeSearch = async (coordinates, searchText) => {
	const url = `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${searchText}&location=${coordinates.lat},${coordinates.lng}&radius=5000&strictbounds&key=${apiKey}`;
	return await axios
		.get(url)
		.then(response => {
			if (response.data?.status === 'OK') {
				return response.data?.predictions;
			} else {
				return response.data?.status;
			}
		})
		.catch(error => {
			return `Something went wrong while searching for '${searchText}'`;
		});
};

/**
 * Filters the result from the suggestions
 * @param {Object} places - search result from placeSearch
 * @return {Object} array containing only the desired output fields
 */
const placeCleanup = async places => {
	return await places.map(place => {
		const termsLength = place.terms.length;
		if (termsLength === 4) {
			return {
				address: place.description,
				barangay: place.terms[0].value,
				municipality: place.terms[1].value,
				province: place.terms[2].value
			};
		} else {
			return {
				address: place.description,
				barangay: place.terms[1].value,
				municipality: place.terms[termsLength - 3].value,
				province: place.terms[termsLength - 2].value
			};
		}
	});
};

/**
 * Handles the request and response
 * @param {Object} req - contains the provided parameters for the request
 * @return {Object || string} array containing only the response for the request or the string that indicates errors that occured during the request
 */
const processRequest = async req => {
	if (
		!req.query?.barangay ||
		!req.query?.municipality ||
		!req.query?.province ||
		!req.query?.searchText
	) {
		return await 'Please provide complete input parameters...';
	} else {
		try {
			const coordinates = await placeCoordinates(
				req.query.barangay,
				req.query.municipality,
				req.query.province
			);
			if (typeof coordinates === 'string') {
				return coordinates;
			}

			const suggestions = await placeSearch(coordinates, req.query.searchText);
			if (typeof suggestions === 'string') {
				return suggestions;
			}

			const filtered = await placeCleanup(suggestions);

			return filtered;
		} catch (error) {
			return error;
		}
	}
};

// localhost:3000
app.get('/', (req, res) => {
	res.send('To use the locator API, go to localhost:3000/locator');
});

// using api at localhost:3000/locator
app.get('/locator', async (req, res) => {
	const response = await processRequest(req);

	console.log('Response:');
	console.log(response);

	res.send(response);
});

app.listen(3000, () => {
	console.log('Listening to localhost:3000');
});

# Locator API

---
## Description
The Locator API uses the Google Maps Platform to get the geocode specific to the `barangay` passed by the client. Then, it returns up to 5 possible results based on the `searchText` provided by the client. The result covers up to 10km search area centered on the specified `barangay`.

---
## Developer
Kent Joash A. Zamudio

---
## How to use
### Required Fields
`barangay` - the barangay or nearby barangay where the to-be-searched place is possibly located

`municipality` - the municipality where the `barangay` is located

`province` - the province where the `municipality` is located

`searchText` - the place to be searched

By providing the barangay, municipality, and province, the search area is narrowed down making the search results more accurate. If any of the required field is missing, an error will occur.
### Format
	http:host.com/locator?barangay=<barangay>&municipality=<municipality>&province=<province>&searchText=<some text here>

### For other Devs
Be sure to have your own API key from Google Map Platform and store this in /res/auth.js with the following format:
```javascript
module.exports=<YOUR_API_KEY>;

```

---
## Third-party API
#### Google Maps Platform
	- Place Search
	- Place Autocomplete

## Other Tools
	- NodeJs
	- ExpressJs
	- Axios
